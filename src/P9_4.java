//////////////////////////////////////////////////////////////////////////////
// Lily Mendoza
// CS 49J
// 10/1/19
// This program creates an array of 3 different objects called appointments
// and ask the user if they would like to add more appointments. If the user
// enter yes, the program will then ask the user for how many appointments
// they plan to add. It will ask the user what kind of appointment they want
// to make and for a description and date if necessary. It will continue this
// for the amount of times the user input. It then asks the user for a date.
// If the user provides an acceptable date, the program prints all the
// appointments scheduled for that date. It loops until user no longer wants
// to check a date and enters n.
/////////////////////////////////////////////////////////////////////////////

import java.util.Scanner;                   // importing package to use Scanner

public class P9_4 {
    // creating superclass Appointment
    public static class Appointment {
        // initializing variables
        String reason;            // reason to hold description of appointment
        int[] date = new int[3];  // date to hold date of appointment
        // empty constructor
        Appointment() {
        }
        // method to take user input to set the reason
        void setReason() {
            // prompting user to enter reason for appointment
            System.out.println("Reason for appointment: ");
            // setting reason to user input
            Scanner r = new Scanner(System.in);
            reason = r.nextLine();
        }
        // method to take user input to set the date
        protected void setDate() {
            // prompting user to enter date of appointment
            System.out.println("Date of appointment: ");
            // setting date to user input
            Scanner d = new Scanner(System.in);
            // converting user input to int array
            String[] strDates = d.nextLine().split("/");
            for (int i = 0; i < 3; i++) {
                date[i] = Integer.parseInt(strDates[i]);
            }
        }
        // method to return appointment and check boolean occursOn
        String getReason(int year, int month, int day) {
            if(occursOn(year, month, day))
                return reason;
            else
                return ("");
        }
        // method to check if appointment is on the same date
        public boolean occursOn(int year, int month, int day) {
            return (year == date[2] && month == date[0] && day == date[1]);
        }
    }
    // creating a subclass OneTime that inherits from Appointment
    public static class OneTime extends Appointment {
        // constructor that takes user input
        OneTime() {
            setReason();        // calling method from Appointment class
            setDate();          // calling method from Appointment class
        }
        // set constructor
        OneTime(String r, String d) {
            // setting reason and date
            reason = r;
            String[] strDates = d.split("/");
            for (int i = 0; i < 3; i++) {
                date[i] = Integer.parseInt(strDates[i]);
            }
        }
    }
    // creating a subclass Daily that inherits from Appointment
    public static class Daily extends Appointment {
        // constructor that takes user input
        Daily() {
            setReason();        // calling method from Appointment class
        }
        // set constructor
        Daily(String r) {
            // setting reason
            reason = r;
        }
        // overwriting method occursOn
        public boolean occursOn(int year, int month, int day) {
            // always true because appointment is daily
            return true;
        }
    }
    // creating a subclass Monthly that inherits from Appointment
    public static class Monthly extends Appointment {
        // constructor that takes user input
        Monthly() {
            setReason();        // calling method form Appointment class
            setDate();          // calling overwritten method
        }
        // set constructor
        Monthly(String r, int d) {
            reason = r;         // setting reason to r
            date[0] = 0;        // setting month to 0 because it is every month
            date[1] = d;        // setting day to d
            date[2] = 0;        // setting year to 0
        }
        // overwriting method setDate
        protected void setDate() {
            // prompting user to enter a day for the appointment
            System.out.println("Day of appointment: ");
            // setting day in array date to user input
            Scanner d = new Scanner(System.in);
            date[1] = d.nextInt();
        }
        // overwriting method occursOn
        public boolean occursOn(int year, int month, int day) {
            // checking that the day is the same as the appointment
            return (day == date[1]);
        }
    }

    public static void main(String[] args) {
        // creating 3 different types of appointments
        OneTime app1 = new OneTime("Haircut", "09/10/19");
        Monthly app2 = new Monthly("Dentist", 4);
        Daily app3 = new Daily("Eat breakfast");
        // creating array of appointments
        Appointment[] myAppointments = {app1, app2, app3};

        // prompting user to enter y or n to add an appointment
        System.out.println("Would you like to add an appointment? (y/n)");
        // sets variable add to user input
        Scanner a = new Scanner(System.in);
        char add = a.next().charAt(0);
        int numOfAppointments;
        // checking if user would like to add
        if (add == 'y') {
            // prompting user to enter number of appointments they would like to add
            System.out.println("How many appointments would you like to add? ");
            // setting variable numOfAppointments to user input
            Scanner n = new Scanner(System.in);
            numOfAppointments = n.nextInt();
        }
        // if user did not enter 'y' numOfAppointments set to 0
        else
            numOfAppointments = 0;
        // constructing empty array of Appointments with length from user input
        Appointment[] userAppointments = new Appointment[numOfAppointments];
        // initializing index i to 0
        int i = 0;
        // constructing while loop to last length from user input
        while (i < numOfAppointments) {
            // prompting user to enter (m/d/o) to represent what type of appointment
            System.out.println("What kind of appointment would you like to make? " +
                    "\nMonthly\nDaily\nOne Time\n(m/d/o): ");
            // setting variable type to user input
            Scanner t = new Scanner(System.in);
            char type = t.next().charAt(0);
            // constructs an appointment based on user input type
            switch (type) {
                case 'm':
                    userAppointments[i] = new Monthly();
                    break;
                case 'd':
                    userAppointments[i] = new Daily();
                    break;
                case 'o':
                    userAppointments[i] = new OneTime();
                    break;
                default:
                    // message to user that they typed an incorrect value
                    System.out.println("Unexpected value: " + type + "Please try again.");
                    // i reset to loop through again
                    i -= 1;
                    break;
            }
            // adding 1 to index
            i++;
        }
        // constructing empty array to combine set appointments and user appointments
        Appointment[] allAppointments = new Appointment[myAppointments.length + userAppointments.length];
        // copying set appointments into empty array
        System.arraycopy(myAppointments,0, allAppointments, 0, myAppointments.length);
        // copying user appointments into array
        System.arraycopy(userAppointments, 0, allAppointments, myAppointments.length, userAppointments.length);
        // initializing boolean check
        boolean check;
        // creating do loop to check appointments
        do {
            // prompting user to enter a date to check for appointments
            System.out.println("Enter a date (00/00/00) to see the appointments: ");
            // setting variable userDate to user input
            Scanner in = new Scanner(System.in);
            String[] userDate = in.nextLine().split("/");
            // searching through appointments and printing the appointments that match
            // the users date
            for (i = 0; i < allAppointments.length; i++) {
                // printing getReason with return type String
                System.out.println(allAppointments[i].getReason(Integer.parseInt(userDate[2]),
                        Integer.parseInt(userDate[0]), Integer.parseInt(userDate[1])));
            }
            // asking user if they would like to continue checking dates
            // y if yes and n if no
            System.out.println("Check another date (y/n): ");
            // setting variable cont to user response
            Scanner c = new Scanner(System.in);
            char cont = c.next().charAt(0);
            switch (cont) {
                case 'y':       // if yes check is set to true and loop continues
                    check = true;
                    break;
                case 'n':       // if no check is set to false and loop breaks
                    check = false;
                    break;
                default:        // if neither yes or no check is set to false and loop breaks
                    System.out.println("Unexpected value: " + cont + "\nTake that as a no.");
                    check = false;
                    break;
            }
        } while (check);
    }
}
